import {callToAction} from './callToAction';
import {contents} from './contents';
import {contacts} from './contacts';
import {features} from './features';
import {footer} from './footer';
import {forms} from './forms';
import {header} from './header';
import {pricings} from './pricings';
import {teams} from './teams';
import {testimonials} from './testimonials';

const thumbnails ={
  callToAction,
  contents,
  contacts,
  features,
  footer,
  forms,
  header,
  pricings,
  teams,
  testimonials
};

export default thumbnails;