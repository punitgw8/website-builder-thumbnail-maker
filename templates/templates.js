const {callToAction} = require('../templates/callToAction');
const {contents} = require('../templates/contents');
const {contacts} = require('../templates/contacts');
const {features} = require('../templates/features');
const {footer} = require('../templates/footer');
const {forms} = require('../templates/forms');
const {header} = require('../templates/header');
const {pricings} = require('../templates/pricings');
const {teams} = require('../templates/teams');
const {testimonials} = require('../templates/testimonials');

const templates ={
  callToAction,
  contents,
  contacts,
  features,
  footer,
  forms,
  header,
  pricings,
  teams,
  testimonials
};

module.exports = { templates };