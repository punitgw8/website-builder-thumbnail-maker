const contents = [
  {
    id: "1",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row justify-content-center">\n      <div class="col col-md-8 text-center">\n        <h1>Froala Design Blocks</h1>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "2",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row justify-content-center">\n      <div class="col col-md-8 text-center">\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in <a href="https://www.froala.com">Bookmarksgrove</a> right at the coast of the Semantics, a large language\n          ocean.</p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "3",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row justify-content-center">\n      <div class="col col-md-8 text-center">\n        <h1>Froala Design Blocks</h1>\n        <p class="lead">Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far <a href="https://www.froala.com">World of Grammar</a>.</p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "4",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row">\n      <div class="col col-sm-10 col-md-8 text-left">\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "5",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row justify-content-end">\n      <div class="col col-sm-10 col-md-8 text-left">\n        <p class="lead">Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far <a href="https://www.froala.com">World of Grammar</a>.</p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "6",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row">\n      <div class="col text-left">\n        <h2>Far far away...</h2>\n\n        <p>Far far away, behind the word mountains, far from the countries <a href="https://www.froala.com">Vokalia</a> and <a href="https://www.froala.com">Consonantia</a>, there live the blind texts. Separated they live in Bookmarksgrove right at the coast\n          of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>\n\n        <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the <a href="https://www.froala.com">Little Blind Text</a>          should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their\n          agency, where they abused her for their.</p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "7",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row">\n      <div class="col text-center">\n        <h1>Froala Design Blocks</h1>\n\n        <div class="row text-left pt-4">\n          <div class="col-12 col-md-6">\n            <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden\n              flows by their place far far away.</p>\n          </div>\n          <div class="col-12 col-md-6">\n            <p class="lead">Separated they live in Bookmarksgrove right at the coast of the Semantics, far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at\n              the coast.</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "8",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row">\n      <div class="col text-center">\n        <h1>Froala Design Blocks</h1>\n        <p class="lead">Subtitle text about launch comes here.</p>\n        <p class="lead">\n          <a href="https://www.froala.com" class="mx-2">Learn More <i class="fas fa-angle-right"></i></a>\n          <a href="https://www.froala.com" class="mx-2">Buy <i class="fas fa-angle-right"></i></a>\n        </p>\n      </div>\n    </div>\n    <div class="row justify-content-center">\n      <div class="col-6">\n        <img alt="image" class="img-fluid mt-5" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/hosting.svg">\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "9",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row">\n      <div class="col text-center">\n        <h1>Froala Design Blocks</h1>\n      </div>\n    </div>\n\n    <div class="row pt-5 justify-content-center align-items-center">\n      <div class="col-4">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/developer.svg">\n      </div>\n      <div class="col-4 offset-1">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/git.svg">\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "10",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row pb-3">\n      <div class="col text-center">\n        <h1>Froala Design Blocks</h1>\n      </div>\n    </div>\n    <div class="row pt-5 justify-content-center align-items-center">\n      <div class="col-3">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/design-life.svg">\n      </div>\n      <div class="col-3 offset-1">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/designer.svg">\n      </div>\n      <div class="col-3 offset-1">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/design-community.svg">\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "11",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row">\n      <div class="col text-center">\n        <h1>Froala Design Blocks</h1>\n      </div>\n    </div>\n\n    <div class="row pt-5 justify-content-center align-items-center">\n      <div class="col-6 col-md-2">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//shapes/1.svg">\n      </div>\n      <div class="col-6 col-md-2 offset-md-1">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//shapes/5.svg">\n      </div>\n      <div class="col-6 col-md-2 mt-4 mt-md-0 offset-md-1">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//shapes/7.svg">\n      </div>\n      <div class="col-6 col-md-2 mt-4 mt-md-0 offset-md-1">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//shapes/4.svg">\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "12",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row align-items-center">\n      <div class="col-12 col-md-6 col-lg-5">\n        <h1>Design Blocks</h1>\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\n      </div>\n      <div class="col-12 col-md-6 ml-md-auto mt-4 mt-md-0">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//shapes/2.svg">\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "13",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row justify-content-center">\n      <div class="col col-md-8 text-center">\n        <img alt="image" class="fdb-icon mb-4" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//icons/gift.svg">\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "14",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row align-items-center">\n      <div class="col-12 col-md-6 mb-4 mb-md-0">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/smile.svg">\n      </div>\n      <div class="col-12 col-md-6 col-lg-5 ml-md-auto text-left">\n        <h1>Froala Blocks</h1>\n        <p class="lead">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>\n        <p><a class="btn btn-secondary mt-4" href="https://www.froala.com">Download</a></p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "15",
    value:
      '<section class="fdb-block" style="background-image: url(https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//shapes/8.svg)">\n  <div class="container">\n    <div class="row align-items-center">\n      <div class="col-12 col-md-6 col-lg-5">\n        <img alt="image" class="fdb-icon" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//icons/github.svg">\n        <h1>Design Blocks</h1>\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\n      </div>\n      <div class="col-10 col-sm-6 m-auto col-md-4 pt-4 pt-md-0">\n        <img alt="image" class="img-fluid rounded-0" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/chat.svg">\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "16",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row justify-content-center">\n      <div class="col-12 col-md-8 text-center">\n        <div class="row justify-content-center pb-5">\n          <div class="col-4 col-sm-3 col-md-2"><img alt="image" class="fdb-icon" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//icons/layers.svg"></div>\n          <div class="col-4 col-sm-3 col-md-2"><img alt="image" class="fdb-icon" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//icons/map.svg"></div>\n          <div class="col-4 col-sm-3 col-md-2"><img alt="image" class="fdb-icon" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//icons/monitor.svg"></div>\n        </div>\n\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "17",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row align-items-center">\n      <div class="col-10 col-sm-6 m-auto col-md-4 pb-4 pb-md-0">\n        <img alt="image" class="img-fluid rounded-0" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/chatting.svg">\n      </div>\n\n      <div class="col-12 ml-auto col-md-6 col-lg-5">\n        <h1>Design Blocks</h1>\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\n        <p><a class="btn btn-primary mt-4 mb-5 mb-md-0" href="https://www.froala.com">Download</a></p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "18",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row align-items-center">\n      <div class="col-12 col-md-12 col-lg-6 col-xl-5">\n        <h1>Design Blocks</h1>\n        <p class="lead mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\n\n        <p><strong>Works in every browser:</strong></p>\n        <p class="h1 text-muted">\n          <i class="fab fa-chrome mr-3"></i>\n          <i class="fab fa-safari mr-3"></i>\n          <i class="fab fa-firefox mr-3"></i>\n          <i class="fab fa-edge"></i>\n        </p>\n      </div>\n      <div class="col-12 col-md-8 m-auto ml-lg-auto mr-lg-0 col-lg-6 pt-5 pt-lg-0">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/browsers.svg">\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "19",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row text-left">\n      <div class="col-12 col-md-6">\n        <img alt="image" class="fdb-icon" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//icons/gift.svg">\n        <h3><strong>Awesome Things</strong></h3>\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Far from the countries Vokalia and Consonantia.</p>\n      </div>\n      <div class="col-12 col-md-6 pt-5 pt-md-0">\n        <img alt="image" class="fdb-icon" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//icons/cloud.svg">\n        <h3><strong>More Awesomeness</strong></h3>\n        <p class="lead">Separated they live in Bookmarksgrove right at the coast of the Semantics, far far away, behind the word mountains, far from the countries <a href="https://www.froala.com">Vokalia and Consonantia</a>, there live the blind texts. </p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "20",
    value:
      '<section class="fdb-block py-0">\n  <div class="container py-5 my-5" style="background-image: url(https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//shapes/10.svg);">\n    <div class="row text-left py-5">\n      <div class="col-12 col-sm-10 m-auto ml-md-5 col-md-8 col-lg-6">\n        <div class="fdb-box">\n          <div class="row justify-content-center">\n            <div class="col-12 col-xl-8 text-center">\n              <h1>Froala Design Blocks</h1>\n              <p class="lead">When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove</p>\n\n              <p class="h3 mt-4"><a href="https://www.froala.com">Learn More <i class="fas fa-angle-right"></i></a></p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "21",
    value:
      '<section class="fdb-block py-0">\n  <div class="container py-5 my-5 bg-r" style="background-image: url(https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//shapes/9.svg);">\n    <div class="row text-left py-5">\n      <div class="col-12 col-md-8 col-lg-6 ml-sm-auto">\n        <div class="fdb-box fdb-touch">\n          <div class="row justify-content-center">\n            <div class="col-12 col-xl-8 text-center">\n              <h1>Froala Design Blocks</h1>\n              <p class="lead">When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove</p>\n\n              <p class="h3 mt-4"><a href="https://www.froala.com" class="btn btn-primary">Register</a></p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "22",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row justify-content-center">\n      <div class="col-12 col-lg-8 col-xl-6 text-center">\n        <img alt="image" width="200" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//logo.png">\n        <h1>Froala Design Blocks</h1>\n        <p class="lead">Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies</p>\n\n        <p class="h3 mt-5"><a href="https://www.froala.com" class="btn btn-primary">Download Now</a></p>\n        <p>Latest Version: 2.3.5</p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "23",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row align-items-center">\n      <div class="col-10 col-sm-6 col-md-5 col-lg-4 m-auto pb-5 pb-md-0">\n        <img alt="image" class="img-fluid rounded-0" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/iphone-hand.svg">\n      </div>\n\n      <div class="col-12 ml-md-auto col-md-7 col-lg-6 pb-5 pb-md-0">\n        <img alt="image" class="fdb-icon" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//icons/gift.svg">\n        <h1>Design Blocks</h1>\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>\n        <p class="mt-4">\n          <a class="btn btn-secondary" href="https://www.froala.com">Download</a>\n          <a class="btn btn-dark ml-3" href="https://www.froala.com">Register</a>\n        </p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "24",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row align-items-center">\n      <div class="col-12 col-md-7 col-lg-5 ml-md-auto">\n        <h1>Design Blocks</h1>\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>\n\n        <p class="mt-5">\n          <img alt="image" height="25" class="mr-4" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//customers/samsung.svg">\n          <img alt="image" height="25" class="mr-4" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//customers/adobe.svg">\n          <img alt="image" height="25" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//customers/amazon.svg">\n        </p>\n      </div>\n\n      <div class="col-10 col-sm-6 col-md-5 col-lg-4 m-auto pt-5 pt-md-0">\n        <img alt="image" class="img-fluid rounded-0" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/customer-survey.svg">\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "25",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row">\n      <div class="col-12">\n        <div class="row justify-content-center pb-5">\n          <div class="col-12 col-lg-8 text-center">\n            <h1>Froala Design Blocks is Open Source and free to use</h1>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class="row justify-content-center">\n      <div class="col-8">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/android.svg">\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "26",
    value:
      '<section class="fdb-block">\n  <div class="col-fill-left" style="background-image: url(https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//people/5.jpg);">\n  </div>\n\n  <div class="container">\n    <div class="row justify-content-end">\n      <div class="col-12 col-md-5 text-center">\n        <h1>Froala Blocks</h1>\n        <p class="lead">When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove</p>\n\n        <p class="mt-4"><a href="https://www.froala.com">Learn More <i class="fas fa-angle-right"></i></a></p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "27",
    value:
      '<section class="fdb-block">\n  <div class="col-fill-right" style="background-image: url(https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//people/6.jpg);">\n  </div>\n\n  <div class="container py-5">\n    <div class="row">\n      <div class="col-12 col-md-5 text-center">\n        <h1>Froala Blocks</h1>\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\n        <p class="mt-4 mb-5"><a class="btn btn-secondary" href="https://www.froala.com">Button</a></p>\n\n        <p>Follow us on</p>\n        <p>\n          <a href="https://www.froala.com" class="mx-2 text-secondary"><i class="fab fa-twitter" aria-hidden="true"></i></a>\n          <a href="https://www.froala.com" class="mx-2 text-secondary"><i class="fab fa-facebook" aria-hidden="true"></i></a>\n          <a href="https://www.froala.com" class="mx-2 text-secondary"><i class="fab fa-instagram" aria-hidden="true"></i></a>\n          <a href="https://www.froala.com" class="mx-2 text-secondary"><i class="fab fa-pinterest" aria-hidden="true"></i></a>\n          <a href="https://www.froala.com" class="mx-2 text-secondary"><i class="fab fa-google" aria-hidden="true"></i></a>\n        </p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "28",
    value:
      '<section class="fdb-block bg-dark">\n  <div class="container">\n    <div class="row text-center">\n      <div class="col-12">\n        <h1>Made with <i class="fas fa-heart text-danger"></i> by Froala</h1>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "29",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row text-center align-items-center">\n      <div class="col-8 col-md-4">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//photos/map-1.jpg">\n      </div>\n\n      <div class="col-4 col-md-2">\n        <div class="row">\n          <div class="col-12">\n            <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//photos/map-2.jpg">\n          </div>\n        </div>\n\n        <div class="row mt-4">\n          <div class="col-12">\n            <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//photos/map-3.jpg">\n          </div>\n        </div>\n      </div>\n\n      <div class="col-12 col-md-6 col-lg-5 ml-auto pt-5 pt-md-0">\n        <img alt="image" class="fdb-icon" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//icons/github.svg">\n        <h1>Design Blocks</h1>\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "30",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row align-items-center">\n      <div class="col-6 col-lg-3">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//photos/fireworks-1.jpg">\n      </div>\n\n      <div class="col-6 col-lg-3">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//photos/fireworks-2.jpg">\n      </div>\n\n      <div class="col-12 col-lg-6 pt-3">\n        <p class="lead text-left">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p>\n        <p class="lead text-left">Right at the coast of the Semantics, a large language ocean. A small river named Duden. <a href="https://www.froala.com">[Read More]</a></p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "31",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row text-left align-items-center">\n      <div class="col-12 col-md-6 col-lg-4">\n        <h2>Your Website</h2>\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>\n        <p class="lead"><a href="https://www.froala.com">Read More</a></p>\n      </div>\n\n      <div class="col-12 col-md-6 col-lg-4 pt-4 pt-md-0">\n        <h2>Amazing Design</h2>\n        <p class="lead">Right at the coast of the Semantics, a large language ocean. A small river named Dude a rge language ocean there live the blind.</p>\n        <p class="lead"><a href="https://www.froala.com">Read More</a></p>\n      </div>\n\n      <div class="col-12 col-md-8 m-auto m-lg-0 col-lg-4 pt-5 pt-lg-0">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/tabs.svg">\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "32",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row text-center">\n      <div class="col-12">\n        <h1>Froala Design Blocks</h1>\n        <p class="h2"><em>Right at the coast of the Semantics, a large language ocean.</em></p>\n      </div>\n    </div>\n\n    <div class="row text-center pt-3 pt-xl-5">\n      <div class="col-12 col-sm-10 m-auto m-md-0 col-md-6">\n        <img alt="image" height="300" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/tenis.svg">\n        <p class="lead pt-3">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>\n      </div>\n\n      <div class="col-12 col-sm-10 m-auto m-md-0 col-md-6 pt-5 pt-md-0">\n        <img alt="image" height="300" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/basketball.svg">\n        <p class="lead pt-3">Right at the coast of the Semantics, a large language ocean. A small river named Duden.</p>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "33",
    value:
      '<section class="fdb-block py-0">\n  <div class="container pt-5 mt-5" style="background-image: url(https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//shapes/9.svg);">\n    <div class="row text-left pt-5">\n      <div class="col-12 col-md-8 col-lg-6">\n        <div class="fdb-box fdb-touch rounded-bottom-0">\n          <h2><strong>Design Blocks</strong></h2>\n          <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics</p>\n          <p class="lead mt-4"><a href="https://www.froala.com">Read More</a></p>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "34",
    value:
      '<section class="fdb-block pb-0" style="background-image: url(https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//hero/purple.svg);">\n  <div class="container">\n    <div class="row text-left justify-content-end">\n      <div class="col-12 col-md-6 col-xl-5">\n        <div class="fdb-box rounded-bottom-0">\n          <h2><strong>Design Blocks</strong></h2>\n          <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>\n\n          <p class="lead mt-4"><a class="btn btn-outline-primary" href="https://www.froala.com">Read More</a></p>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
  {
    id: "35",
    value:
      '<section class="fdb-block">\n  <div class="container">\n    <div class="row text-left align-items-center">\n      <div class="col-10 col-sm-6 m-auto m-lg-0 col-lg-4">\n        <img alt="image" class="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/imgs//draws/opened.svg">\n      </div>\n\n      <div class="col-12 col-lg-7 offset-lg-1 pt-4 pt-lg-0">\n        <h1>Froala Design Blocks</h1>\n        <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>\n\n        <div class="row mt-5">\n          <div class="col-12 col-sm-6">\n            <h3><strong>Open Source</strong></h3>\n            <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>\n          </div>\n\n          <div class="col-12 col-sm-6 pt-3 pt-sm-0">\n            <h3><strong>Bootstrap</strong></h3>\n            <p class="lead">Right at the coast of the Semantics, a large language ocean. A small river named Duden.</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n',
  },
];

module.exports = { contents };
