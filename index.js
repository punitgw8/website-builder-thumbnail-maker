const express = require('express');
const fs = require("fs");
const app=express();
const {templates} = require("./templates/templates");
const {boiler} = require("./templates/boiler");
const nodeHtmlToImage = require('node-html-to-image');

const category=["callToAction","contents","contacts","features","footer","forms","header","pricings","teams","testimonials"];

const fileWrite=()=>{
  for(let i=0;i<category.length;i++){
    let importContent =``;
    let exportContent = `export const ${category[i]} = [`
    for(let j=0;j<templates[category[i]].length;j++){
      importContent += `import image${j} from './${category[i]}/image${j}.jpg';`
      exportContent += `image${j}`;
      exportContent += templates[category[i]].length-1==j? '];': ',';
    }
    let content = importContent + exportContent;
    fs.writeFile(`./Thumbnails/${category[i]}.js`,content, err=>{
      if(err){
        console.log(err);
        return
      }else{
        console.log(category[i]);
      }
    })
  }

}
const imageCreator=(j,i)=>{
  return new Promise((resolve,reject)=>{nodeHtmlToImage({
    output: `./Thumbnails/${category[i]}/image${j}.jpg`,
    html: boiler[0]+templates[category[i]][j].value+boiler[1]
  })
    .then(()=>{
      console.log("created");
      resolve()
    })
    .catch(()=>{
      console.log("not created");
      reject()
    });
  });
};
const imageCreationWrite=async ()=>{
  for(let i=0;i<category.length;i++){
    //console.log(templates[category[i]]);
    for(let j=0;j<templates[category[i]].length;j++){
      await imageCreator(j,i);
    }
  }
}
app.listen(5500, () => {
  //console.log(templates);
  console.log("Currently on port 5500");
  imageCreationWrite();
  fileWrite();
});


